﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestStatus
{
    public Quest Quest;
    public QuestProgression Progression;
    
    public QuestStatus(Quest quest)
    {
        Quest = quest;
        Progression = new QuestProgression(quest);
    }

    public QuestStatus(Quest quest, QuestProgression progression)
    {
        Quest = quest;
        Progression = progression;
    }
    
    public void UpdateKillGoals(int enemyID)
    {
        foreach (var goal in Progression.Goals)
        {
            if (goal is KillProgressGoal && !goal.Finished && goal.ID == enemyID)
            {
                (goal as KillProgressGoal).Current++;

                if ((goal as KillProgressGoal).Required <= (goal as KillProgressGoal).Current)
                {
                    goal.Finished = true;
                }
                else
                {
                    goal.Finished = false;
                }

                UpdateProgressComplete();
            }
        }
    }
    
    public void UpdateTutorialGoals(int actionID)
    {
        foreach (var goal in Progression.Goals)
        {
            if (goal is TutorialProgressGoal && !goal.Finished && goal.ID == actionID)
            {
                goal.Finished = true;

                UpdateProgressComplete();
            }
        }
    }

    
    public void UpdateNPCGoals(int npcID)
    {
        foreach (var goal in Progression.Goals)
        {
            if (goal is ActivateNPCProgressGoal && !goal.Finished && goal.ID == npcID)
            {
                goal.Finished = true;

                UpdateProgressComplete();
            }
        }
    }
    
    public void UpdateZoneGoals(int zoneID)
    {
        foreach (var goal in Progression.Goals)
        {
            if (goal is ActivateZoneProgressGoal && !goal.Finished && goal.ID == zoneID)
            {
                goal.Finished = true;

                UpdateProgressComplete();
            }
        }
    }
    
    public void UpdateProgressComplete()
    {
        UIHUD.Instance.DisplayQuests();
        if (Progression.CurrentState == QuestProgression.State.Finished)
        {
            return;
        }

        foreach (var goal in Progression.Goals)
        {
            if (!goal.Finished)
            {
                Progression.CurrentState = QuestProgression.State.Accepted;
                return;
            }
        }

        Progression.CurrentState = QuestProgression.State.Completed;
        EventManager.TriggerEvent(new QuestEvent(QuestEvent.EventType.Completed, Quest));
    }
}

[Serializable]
public class QuestProgression
{
    public List<ProgressGoal> Goals;

    public enum State { Accepted, Completed, Finished }
    public State CurrentState;

    public QuestProgression(Quest quest)
    {
        Goals = new List<ProgressGoal>();

        foreach (KillGoal goal in quest.EnemyAmount)
        {
            Goals.Add(new KillProgressGoal(goal));
        }
        foreach (ActivateNPCGoal goal in quest.ActivateNpcID)
        {
            Goals.Add(new ActivateNPCProgressGoal(goal));
        }
        foreach (ActivateZoneGoal goal in quest.ActivateZoneID)
        {
            Goals.Add(new ActivateZoneProgressGoal(goal));
        }
        foreach (TutorialGoal goal in quest.Actions)
        {
            Goals.Add(new TutorialProgressGoal(goal));
        }

        CurrentState = State.Accepted;
    }
}
