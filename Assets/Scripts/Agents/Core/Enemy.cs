﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using UnityEngine.Experimental.PlayerLoop;

public class Enemy : Agent, EventListener<DialogueEvent>
{
    protected const float FADE_DEATH_DURATION = 1f;
    public float FadeDeathTime = 1f;

    protected MeshRenderer m_MeshRenderer;
    protected BehaviorTree m_Behavior;
    
    public bool CurrentAIBehaviour { get; set; }

    protected override void Awake()
    {
        m_Behavior = GetComponent<BehaviorTree>();
        m_MeshRenderer = GetComponent<MeshRenderer>();

        base.Awake();

        if (m_Behavior != null)
        {
            CurrentAIBehaviour = m_Behavior.enabled;
        }
    }

    private void Start()
    {
        UpdateBaseStats();
        
        if(m_Health != null) m_Health.Initialize();
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        
        this.EventStartListening<DialogueEvent>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        
        this.EventStopListening<DialogueEvent>();
    }

    public void EnableAI()
    {
        CurrentAIBehaviour = true;
        
        m_Behavior.EnableBehavior();
    }

    public override void UpdateHealthUI()
    {
        
    }

    public override void UpdateStaminaUI()
    {
        
    }

    public void DisableAI()
    {
        CurrentAIBehaviour = false;
        
        FreedomState.ChangeState(AgentStates.AgentMovementConditions.Immobilized);
        
        m_Behavior.DisableBehavior();
    }

    public void PauseAI()
    {
        FreedomState.ChangeState(AgentStates.AgentMovementConditions.Immobilized);
        
        m_Behavior.DisableBehavior();
    }

    public void ResumeAI()
    {
        if (CurrentAIBehaviour)
        {
            FreedomState.ChangeState(AgentStates.AgentMovementConditions.Free);
            m_Behavior.EnableBehavior();
        }
    }
    
    public override IEnumerator OnDamaged(Transform damager)
    {
        ConditionState.ChangeState(AgentStates.AgentConditions.Damaged);

        if (m_Health.CurrentHealth <= 0)
        {
            yield return StartCoroutine(OnDeath(damager));
        }
        else
        {
            yield return new WaitForSeconds(m_Health.DamageDuration);
            ConditionState.ChangeState(AgentStates.AgentConditions.Normal);
        }
    }

    public override IEnumerator OnDeath(Transform damager)
    {
        if (m_Hitbox != null) m_Hitbox.EnableBox(false);
        ConditionState.ChangeState(AgentStates.AgentConditions.Dead);
        
        if (damager != null)
        {
            EventManager.TriggerEvent(new XpEvent(XpEvent.XPMethods.Add, (Data as EnemySO).XPOnDeath));
            EventManager.TriggerEvent(new CoinsEvent(CoinsEvent.CoinsMethods.Add, (Data as EnemySO).MoneyAmount));
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Kill, Data.Id));
            EventManager.TriggerEvent(new AchievementProgressEvent(AchievementProgressEvent.EventType.Kill, Data.Id));
        }
        
        yield return new WaitForSeconds(FadeDeathTime);

        StartCoroutine(FadeEnemyOnDeath());
    }
    
    public virtual void OnEvent(DialogueEvent eventType)
    {
        switch (eventType.Type)
        {
            case DialogueEvent.EventType.Start:
                PauseAI();
                break;
            case DialogueEvent.EventType.End:
                ResumeAI();
                break;
        }
    }
    
    public virtual IEnumerator FadeEnemyOnDeath()
    {
        yield return StartCoroutine(FadeMaterial(m_MeshRenderer, FADE_DEATH_DURATION, new Color(m_MeshRenderer.material.color.r, m_MeshRenderer.material.color.g, m_MeshRenderer.material.color.b, 0f)));
        
        gameObject.SetActive(false);
    }

    public IEnumerator FadeMaterial(MeshRenderer renderer, float duration, Color color)
    {
        if (renderer == null)
        {
            yield break;
        }

        float alpha = renderer.material.color.a;

        float t = 0f;
        while (t < 1.0f)
        {
            if (renderer == null)
            {
                yield break;
            }
            
            Color newColor = new Color(color.r,color.g,color.b, Mathf.SmoothStep(alpha, color.a, t));
            renderer.material.color = newColor;

            t += Time.deltaTime / duration;

            yield return null;
        }
        
        Color finalColor = new Color(color.r, color.g, color.b, Mathf.SmoothStep(alpha, color.a, t));
        if (renderer != null)
        {
            renderer.material.color = finalColor;
        }
    }
    
    protected override void UpdateBaseStats()
    {
        m_BaseStats.Health = Data.Stats.Health;
        m_BaseStats.Attack = Data.Stats.Attack;
        m_BaseStats.Defense = Data.Stats.Defense;
        m_BaseStats.WalkingSpeed = Data.Stats.WalkingSpeed;
        m_BaseStats.RunningSpeed = Data.Stats.RunningSpeed;

        CurrentStats = new Stats(m_BaseStats);
    }

   
}
