﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class HealthEvent : UnityEvent<Transform> { }

public struct DamageProperties
{
    public DamageValues DamageValues;
    public Transform Damager;
    public CombatTechnique Technique;

    public DamageProperties(DamageValues damageValues, Transform damager, CombatTechnique technique = null)
    {
        DamageValues = damageValues;
        Damager = damager;
        Technique = technique;
    }
}

public class Health : MonoBehaviour
{
    [Header("Health")]
    public bool InfiniteHealth = false;
    public int MaximumHealth = 10;

    public float DamageDuration = 1f;

    [ReadOnly]
    public int CurrentHealth;

    
    protected HealthBar m_HealthBar;
    protected HurtboxBehaviour m_Hurtbox;

    [Header("Events")]
    public HealthEvent OnDamageEvent;
    public HealthEvent OnDeathEvent;
    public HealthEvent OnKillEvent;

    protected virtual void Awake()
    {
        m_HealthBar = GetComponentInChildren<HealthBar>();
        m_Hurtbox = GetComponentInChildren<HurtboxBehaviour>();
    }
    void Start()
    {
        CurrentHealth = MaximumHealth;
    }

    public virtual void HealDamage(int healAmmount)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth + healAmmount, 0, MaximumHealth);
    }

    public virtual void Restore()
    {
        CurrentHealth = MaximumHealth;
        
        if (m_Hurtbox != null) m_Hurtbox.EnableBox(true);
    }

    public virtual void Restore(float value)
    {
        CurrentHealth = (int)(MaximumHealth * Mathf.Clamp(value, 0f, 1f));
    }

    public virtual void Kill()
    {
        CurrentHealth = 0;
        if (m_HealthBar != null) m_HealthBar.HideBar();

        OnKillEvent.Invoke(null);
    }

    public virtual int TakeDamage(DamageProperties damageProperties)
    {
        return TakeDirectDamage(damageProperties);
    }

    public virtual int TakePercentageDamage(DamageProperties damageProperties)
    {
        damageProperties.DamageValues.Damage = (int)(MaximumHealth * (damageProperties.DamageValues.Damage / 100f));

        return TakeDirectDamage(damageProperties);
    }

    public virtual int TakeDirectDamage(DamageProperties damageProperties)
    {
        if (CurrentHealth <= 0)
        {
            return 0;
        }

        int finalDamage = CalculateDamageTaken(damageProperties.DamageValues.Damage);
        if (!InfiniteHealth)
        {
            CurrentHealth = Mathf.Clamp(CurrentHealth - finalDamage, 0, MaximumHealth);
        }
        
        OnDamageEvent.Invoke(damageProperties.Damager);

        if (CurrentHealth <= 0)
        {
            if (m_Hurtbox != null) m_Hurtbox.EnableBox(false);
            OnDeathEvent.Invoke(damageProperties.Damager);
        }

        return finalDamage;
    }

    protected virtual int CalculateDamageTaken(int damage)
    {
        return damage;
    }
}
