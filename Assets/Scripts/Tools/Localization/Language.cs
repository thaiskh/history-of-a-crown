﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Lang", menuName = "G1/Localization/Language")]
public class Language : ScriptableObject
{
    public string Locale;
    public string NameKey;
}
