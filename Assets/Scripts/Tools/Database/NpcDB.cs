﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "NpcDB", menuName = "G1/Database/NpcDB")]
public class NpcDB : AbstractDatabase<NPC>
{
    private static NpcDB m_Instance = null;

    public static NpcDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(NPC t)
    {
#if UNITY_EDITOR
        t.name = "NPC" + t.Id;
        AssetDatabase.AddObjectToAsset(t, this);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    protected override void OnRemoveObject(NPC t)
    {
#if UNITY_EDITOR
        AssetDatabase.RemoveObjectFromAsset(t);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (NpcDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/NpcDB.asset", typeof(NpcDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/agent/npcs");
            m_Instance = bundle.LoadAsset<NpcDB>("NpcDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
