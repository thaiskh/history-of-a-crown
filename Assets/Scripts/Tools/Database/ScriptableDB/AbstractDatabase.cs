﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractDatabase<T> : ScriptableObject where T : IDatabaseAsset
{
    [SerializeField]
    protected List<T> m_Objects;

    protected abstract void OnAddObject(T t);
    protected abstract void OnRemoveObject(T t);

    public int RowCount { get { if (m_Objects == null) return 0; else return m_Objects.Count; } }

    protected List<T> Objects
    {
        get
        {
            if (m_Objects == null)
            {
                m_Objects = new List<T>();
            }
            return m_Objects;
        }
    }

    public void Add(T t)
    {
        Objects.Add(t);
        OnAddObject(t);
    }

    public void Remove(T t)
    {
        Objects.Remove(t);
        OnRemoveObject(t);
    }

    public void RemoveAt(int index)
    {
        var obj = Objects[index];
        Objects.RemoveAt(index);
        OnRemoveObject(obj);
    }

    public void Replace(int index, T t)
    {
        var old = Objects[index];
        Objects[index] = t;
        OnRemoveObject(old);
        OnAddObject(t);
    }

    public bool Contains(T t)
    {
        return Objects.Contains(t);
    }

    public int Count
    {
        get { return Objects.Count; }
    }

    public List<int> GetIds()
    {
        List<int> ids = new List<int>();

        for (int i = 0; i < Count; i++)
        {
            ids.Add(Objects[i].Id);
        }

        return ids;
    }

    public T GetAtIndex(int index)
    {
        if (this.Count > 0)
        {
            if (index >= 0 && index < this.Count)
                return Objects[index];
            return default(T);
        }
        return default(T);
    }

    public T GetById(int id)
    {
        for (int i = 0; i < Count; i++)
        {
            var asset = GetAtIndex(i);
            if (asset.Id == id)
            {
                return (T)GetAtIndex(i);
            }
        }
        return default(T);
    }

    public int GetIndex(int id)
    {
        for (int i = 0; i < Count; i++)
        {
            if (GetAtIndex(i).Id == id)
            {
                return i;
            }
        }

        return -1;
    }

    public bool ContainsDuplicateId()
    {
        for (int i = 0; i < Count - 1; i++)
        {
            var asset1 = GetAtIndex(i);
            for (int j = i + 1; j < Count; j++)
            {
                var asset2 = GetAtIndex(j);
                if (asset1.Id == asset2.Id)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public int GetFirstAvalibleId()
    {
        if (Count <= 0)
        {
            return 1;
        }
        else
        {
            int target = 1;
            bool found = false;
            while (!found)
            {
                found = true;
                for (int i = 0; i < Count; i++)
                {
                    if (GetAtIndex(i).Id == target)
                    {
                        found = false;
                        target++;
                        break;
                    }
                }
            }
            return target;
        }
    }
}

