﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "AchievementDB", menuName = "G1/Database/AchievementDB")]
public class AchievementDB : AbstractDatabase<Achievement>
{
    private static AchievementDB m_Instance = null;

    public static AchievementDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(Achievement t)
    {
#if UNITY_EDITOR
        t.name = "Achievement" + t.Id;
        AssetDatabase.AddObjectToAsset(t, this);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    protected override void OnRemoveObject(Achievement t)
    {
#if UNITY_EDITOR
        AssetDatabase.RemoveObjectFromAsset(t);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (AchievementDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/AchievementDB.asset", typeof(NpcDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/achievements");
            m_Instance = bundle.LoadAsset<AchievementDB>("AchievementDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
