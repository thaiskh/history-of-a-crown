﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Quest", menuName = "G1/Diary/Quest System/Quest")]
public class Quest : ScriptableObject, IDatabaseAsset
{
    public enum Type
    {
        Main, Secondary
    }

    public int QuestID;

    public string QuestNameTextKey;
    public string QuestDescriptionTextKey;
    public string QuestShortDescriptionTextKey;
    public Type QuestType;
    public UnlockConditions QuestConditions;
    public Reward QuestReward;
    
    public List<KillGoal> EnemyAmount;
    public List<ActivateNPCGoal> ActivateNpcID;
    public List<ActivateZoneGoal> ActivateZoneID;
    public List<TutorialGoal> Actions;

    public int Id { get { return QuestID; } set { QuestID = value; } }
    public string Name { get { return QuestNameTextKey; } set { QuestNameTextKey = value; } }
    
    
}

public abstract class QuestGoal
{
    public int ID;
    public string Name;

    public QuestGoal(int id, string name)
    {
        ID = id;
        Name = name;
    }
}

[Serializable]
public class TutorialGoal : QuestGoal
{
    public TutorialGoal(int actionID, string name) : base(actionID,name)
    {
       
    }
}

[Serializable]
public class KillGoal : QuestGoal
{
    public int RequiredAmount;

    public KillGoal(int enemyID, string name, int required) : base(enemyID, name)
    {
        RequiredAmount = required;
    }
}

[Serializable]
public class ActivateNPCGoal : QuestGoal
{
    public ActivateNPCGoal(int npcID, string name) : base(npcID, name)
    {

    }
}

[Serializable]
public class ActivateZoneGoal : QuestGoal
{
    public ActivateZoneGoal(int areaID, string name) : base(areaID, name)
    {

    }
}

[Serializable]
public struct ItemReward
{
    public int Item;
    public int ItemNumber;
}

[Serializable]
public struct Reward
{
    public int MoneyReward;
    public int ExperienceReward;
    public List<ItemReward> ItemRewards;
}

[Serializable]
public struct UnlockConditions
{
    public List<int> previousQuest;
    public int RequiredLevel;
}
