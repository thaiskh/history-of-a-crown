﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

[CustomEditor(typeof(CancelableButton))]
public class CancelableButtonEditor : ButtonEditor
{
    public SerializedProperty cancelEventProp;

    protected override void OnEnable()
    {
        base.OnEnable();

        cancelEventProp = serializedObject.FindProperty("onCancel");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.PropertyField(cancelEventProp);
        serializedObject.ApplyModifiedProperties();
    }
}
