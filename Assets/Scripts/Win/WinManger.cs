﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.SceneManagement;

public class WinManger : MonoBehaviour
{
    public GameObject Rey;
    
    private void Update()
    {
        if (!Rey.activeSelf)
        {
            StartCoroutine(Counter());
        }
    }

    private IEnumerator Counter()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("Scenes/Creditos");
    }
}
