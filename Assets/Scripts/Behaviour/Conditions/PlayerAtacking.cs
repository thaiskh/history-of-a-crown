using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class PlayerAtacking : Conditional
{
    private Agent _Agent;
    private GameObject _Player;

    public override void OnStart()
    {
        _Player = GameObject.FindGameObjectWithTag("Player");
        _Agent = _Player.GetComponent<Agent>();
    }

    public override TaskStatus OnUpdate()
    {
        if (_Player==null)
        {
            _Player = GameObject.FindGameObjectWithTag("Player");
            _Agent = _Player.GetComponent<Agent>();
        }
        if (_Agent.CombatState.CurrentState == AgentStates.CombatStates.Attacking) {
            return TaskStatus.Success;
        } else
        {
            return TaskStatus.Failure;
        }
    }
}