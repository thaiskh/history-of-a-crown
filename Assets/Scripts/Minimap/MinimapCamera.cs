﻿using UnityEngine;

public class MinimapCamera : MonoBehaviour
{
    public Transform PlayerTransform;
    public float MinimapDistance = 150f;
    
    void Update() {
        PlayerTransform = FindObjectOfType<Character>().gameObject.transform;
        transform.position = PlayerTransform.position + new Vector3(0,MinimapDistance,0);
    }
}
