﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject, IDatabaseAsset
{
    public int ID;
    public int MaxQuantity;

    public string ItemName;
    public string Description;
    public Sprite Icon;
    
    public int Id { get { return ID; } set { ID = value; } }
    public string Name { get { return ItemName; } set { ItemName = value; } }
    public int Quantity { get { return MaxQuantity; } set { MaxQuantity = value; } }
    
    public virtual string GetDescription()
    {
        return LocalizationManager.Instance["ITEM", Description];
    }
}
