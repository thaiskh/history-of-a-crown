﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpeedPotion", menuName = "G1/Item/Consumable/Speed Potion")]
public class SpeedPotion : ConsumableItem, IBuy, ISell
{
    public int BuyValue;
    public int SellValue;

    [Header("Effect")]
    public int Speed;
    public float Duration;

    public int BuyPrice => BuyValue;
    public int SellPrice => SellValue;
    
    public override bool Usable(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();

        return (effects != null);
    }
    
    public override bool Use(Transform target)
    {
        AgentEffects effects = target.GetComponent<AgentEffects>();

        effects.AddEffect(new SpeedBuff(Duration, false, Speed));

        return true;
    }

    public override string GetDescription()
    {
        string message = base.GetDescription();

        message = message.Replace("%replace%", Speed.ToString());
        message = message.Replace("%duration%", Duration.ToString());

        return message;
    }
}
