﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIShopMainMenu : MonoBehaviour
{
   public GameObject FirstGameObject;
   public TextMeshProUGUI NpcText;
   
   public Transform Opener { get; set; }

   private NPCShop m_NpcShop;

   private CanvasGroup m_MenuCanvasGroup;
   private GameObject m_PreviousSelected;

   private void Awake()
   {
      m_MenuCanvasGroup = GetComponent<CanvasGroup>();
   }

   public void ShowMenu()
   {
      UIHUD.Instance.EnableHUD(false);
      if (m_PreviousSelected != null)
      {
         EventSystem.current.SetSelectedGameObject(m_PreviousSelected);
      }
      else
      {
         EventSystem.current.SetSelectedGameObject(FirstGameObject);
      }

      m_MenuCanvasGroup.interactable = true;
   }

   public void OpenMenu(Transform opener, NPCShop npcShop)
   {
      Opener = opener;
      m_NpcShop = npcShop;
      ShowMenu();
      LoadText();
   }
   
   public void CloseMenu()
   {
      UIHUD.Instance.EnableHUD(true);
      m_NpcShop.CloseShop(Opener);

      Opener = null;
      m_NpcShop = null;

      GUIManager.Instance.CloseMenuShopScreen();
   }
   
   public void HideMenuNoAnim()
   {
      m_MenuCanvasGroup.alpha = 0;
      m_MenuCanvasGroup.interactable = false;
   }
   
   public void ShowMenuNoAnim()
   {
      UIHUD.Instance.EnableHUD(false);
      m_MenuCanvasGroup.alpha = 1;
      m_MenuCanvasGroup.interactable = true;
      if (m_PreviousSelected != null)
      {
         EventSystem.current.SetSelectedGameObject(m_PreviousSelected);
      }
      else
      {
         EventSystem.current.SetSelectedGameObject(FirstGameObject);
      }
   }
   
   public void OpenShop()
   {            
      m_PreviousSelected = EventSystem.current.currentSelectedGameObject;
      GUIManager.Instance.OpenShopScreen(m_NpcShop.CurrentShop, Opener);
   }
   
   private void LoadText()
   {
      NpcText.text = LocalizationManager.Instance["DIALOGUE", m_NpcShop.MessageText];
   }
}
