﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIInventory : Singleton<GUIInventory>, ICancelHandler, IMoveHandler
{
    public InventoryOption InventorySlot;
    public GameObject OverlayMessageScreen;

    public Transform Layout;
    public TextMeshProUGUI Category;

    [Header("Item Description")] 
    public Image ItemIcon;
    public TextMeshProUGUI ItemName;
    public TextMeshProUGUI ItemDescription;
    
    public Transform Opener { get; set; }
    
    private Inventory.Category m_SelectedCategory;
    private GameObject m_TmpOverlayMessageScreen;
    
    private CanvasGroup m_CanvasGroup;

    protected override void Awake()
    {
        base.Awake();

        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    public void OpenInventory(Transform opener)
    {
        Opener = opener;

        DrawContent(Inventory.Category.All);
        GUIStats.Instance.Init(opener);

        m_CanvasGroup.alpha = 1;
        m_CanvasGroup.interactable = true;
        m_CanvasGroup.blocksRaycasts = true;
    }

    public void CloseInventory()
    {
        Opener = null;

        EraseContent();
        
        if (m_TmpOverlayMessageScreen != null)
        {
            Destroy(m_TmpOverlayMessageScreen);
        }

        EventSystem.current.SetSelectedGameObject(null);
        
        m_CanvasGroup.alpha = 0;
        m_CanvasGroup.interactable = false;
        m_CanvasGroup.blocksRaycasts = false;
    }

    protected void DrawContent(Inventory.Category category)
    {
        Category.text = LocalizationManager.Instance["ITEM", "CATEGORY_" + category.ToString().ToUpper()];

        List<ListedItem> items = Inventory.Instance.GetCategoryItems(category);
        
        for (int i = 0; i < 24; i++)
        {
            InventoryOption option = Instantiate(InventorySlot, Layout);
            if (i < items.Count)
            {
                option.Init(items[i]);
            }
            else
            {
                option.interactable = false;
                option.ItemIcon.gameObject.SetActive(false);
                option.Quantity.gameObject.SetActive(false);
            }
        }
        
        if (Layout.childCount > 0)
        {
            EventSystem.current.SetSelectedGameObject(Layout.GetChild(0).gameObject);
        }
        else
        {
            EventSystem.current.SetSelectedGameObject(gameObject);
        }
    }
    
    protected void EraseContent()
    {
        for (int i = Layout.childCount - 1; i >= 0; i--)
        {
            Transform child = Layout.GetChild(i);

            child.SetParent(null);
            Destroy(child.gameObject);
        }
    }
    
    public void ReloadContent()
    {
        for (int i = Layout.childCount - 1; i >= 0; i--)
        {
            InventoryOption child = Layout.GetChild(i).GetComponent<InventoryOption>();
            if (child.Item != null)
            {
                if (child.Item.Quantity > 0)
                {
                    child.Refresh();
                }
                else
                {
                    child.ItemIcon.gameObject.SetActive(false);
                    child.Quantity.gameObject.SetActive(false);
                    child.interactable = false;
                    EventSystem.current.SetSelectedGameObject(Layout.GetChild(0).gameObject);
                    // child.transform.SetParent(null);
                    // Destroy(child.gameObject);
                    // if (i > 0)
                    // {
                    //     i--;
                    // }
                    //
                    // if (i < Layout.childCount)
                    // {
                    //     EventSystem.current.SetSelectedGameObject(Layout.GetChild(i).gameObject);
                    // }
                    // else
                    // {
                    //     EventSystem.current.SetSelectedGameObject(gameObject);
                    // }
                }
            }
        }
    }
    
    public void ReloadContentAtChangeCategory()
    {
        EraseContent();
        DrawContent(m_SelectedCategory);
    }
    
    public void NextCategory()
    {
        m_SelectedCategory++;

        if (m_SelectedCategory > Inventory.Category.Usable)
        {
            m_SelectedCategory = Inventory.Category.All;
        }

        ReloadContentAtChangeCategory();
    }
    
    public void PreviousCategory()
    {
        m_SelectedCategory--;

        if (m_SelectedCategory < Inventory.Category.All)
        {
            m_SelectedCategory = Inventory.Category.Usable;
        }

        ReloadContentAtChangeCategory();
    }
    
    public void OnCancel(BaseEventData eventData)
    {
        GUIManager.Instance.CloseInventory();
    }
    
    public void OnMove(AxisEventData eventData)
    {
        if (eventData.moveDir == MoveDirection.Left)
        {
            PreviousCategory();
        }
        else if (eventData.moveDir == MoveDirection.Right)
        {
            NextCategory();
        }
    }

    public void ShowOverlayMessage(string message, TextAnchor anchor, GameObject previousObject)
    {
        if (m_TmpOverlayMessageScreen != null)
        {
            Destroy(m_TmpOverlayMessageScreen);
        }

        m_TmpOverlayMessageScreen = Instantiate(OverlayMessageScreen, transform);
        m_TmpOverlayMessageScreen.GetComponentInChildren<OverlayMessageSystem>().NewMessage(message, anchor, previousObject);
    }
}
