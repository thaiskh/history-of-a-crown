﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GUIInGameSettings : Singleton<GUIInGameSettings>
{
    [Header("Canvas Groups")] 
    public CanvasGroup PauseCanvasGroup;
    public CanvasGroup OptionsCanvasGroup;
    public CanvasGroup AudioCanvasGroup;
    
    [Header("Music Sliders")]
    public Slider MusicSlider;
    public Slider SfxSlider;
    
    [Header("First Menu Elements")]
    public GameObject MenuFirstSelectedObject;
    public GameObject OptionsFirstSelectedObject;
    public GameObject AudioMenuFirstSelectedObject;
    
    public bool Open { get; set; }


    public void StartValues()
    {
        MusicSlider.value = SoundManager.Instance.MusicVolume;
        SfxSlider.value = SoundManager.Instance.SfxVolume;
    }
    public void ShowSettingsPanel()
    {
        StartValues();
        
        PauseCanvasGroup.alpha = 0;
        PauseCanvasGroup.interactable = false;
        PauseCanvasGroup.blocksRaycasts = false;
        OptionsCanvasGroup.alpha = 1;
        OptionsCanvasGroup.interactable = true;
        OptionsCanvasGroup.blocksRaycasts = true;

        EventSystem.current.SetSelectedGameObject(OptionsFirstSelectedObject);

        Open = true;
    }

    public void CloseSettingsPanel()
    {
        PauseCanvasGroup.alpha = 1;
        PauseCanvasGroup.interactable = true;
        PauseCanvasGroup.blocksRaycasts = true;   
        OptionsCanvasGroup.alpha = 0;
        OptionsCanvasGroup.interactable = false;
        OptionsCanvasGroup.blocksRaycasts = false;
        
        EventSystem.current.SetSelectedGameObject(MenuFirstSelectedObject);
        
        GameManager.Instance.SaveSettings();

        Open = false;            
    }
    
    public void UnPausedMenu()
    {           
        AudioCanvasGroup.alpha = 0;
        AudioCanvasGroup.interactable = false;
        AudioCanvasGroup.blocksRaycasts = false;
    }
    
    public void OpenAudioMenu()
    {
        OptionsCanvasGroup.alpha = 0;
        OptionsCanvasGroup.interactable = false;
        OptionsCanvasGroup.blocksRaycasts = false;
        AudioCanvasGroup.alpha = 1;
        AudioCanvasGroup.interactable = true;
        AudioCanvasGroup.blocksRaycasts = true;
        
        EventSystem.current.SetSelectedGameObject(AudioMenuFirstSelectedObject);
    }

    public void CloseAudioMenu()
    {            
        AudioCanvasGroup.alpha = 0;
        AudioCanvasGroup.interactable = false;
        AudioCanvasGroup.blocksRaycasts = false;
        OptionsCanvasGroup.alpha = 1;
        OptionsCanvasGroup.interactable = true;
        OptionsCanvasGroup.blocksRaycasts = true;

        EventSystem.current.SetSelectedGameObject(OptionsFirstSelectedObject);            
    }
    
    public void ChangeMusicValue()
    {
        SoundManager.Instance.SetMusicVolume(MusicSlider.value);
    }

    public void ChangeSFXValue()
    {
        SoundManager.Instance.SetSFXVolume(SfxSlider.value);
    }
}
