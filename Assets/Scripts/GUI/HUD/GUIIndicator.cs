﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIIndicator : MonoBehaviour, EventListener<LevelingEvents>
{
    public TextMeshProUGUI Player;

    public SliderBar HP;

    public Image Portrait;

    public GUIStamina Stamina;

    public TextMeshProUGUI LevelText;

    public TextMeshProUGUI CurrentHP;

    public TextMeshProUGUI TotalHP;

    private AgentHealth m_Health;
    private AgentStamina m_Stamina;
    private CharacterSO m_CharacterData;
    
    public Character Character { get; set; }

    private void OnEnable()
    {
        this.EventStartListening<LevelingEvents>();
    }

    private void OnDisable()
    {
        this.EventStopListening<LevelingEvents>();
    }

    public GUIIndicator Init(Character character)
    {
        gameObject.SetActive(true);

        Character = character;
        m_CharacterData = Character.Data as CharacterSO; 

        m_Health = character.GetComponent<AgentHealth>();
        m_Stamina = character.GetComponent<AgentStamina>();

        Player.text = GameManager.Instance.PlayerName;
        
        Portrait.sprite = m_CharacterData.Icon;

        LevelText.text = MatchManager.Instance.LevelSystem.Level.ToString();
        
        return this;
    }

    public void HideIndicator()
    {
        gameObject.SetActive(false);
    }

    public void UpdateIndicator()
    {
        UpdateHealthBar();
        UpdateStamina();
    }

    public void UpdateHealthBar()
    {
        HP.UpdateBar(m_Health.CurrentHealth, 0f, m_Health.MaximumHealth);
        CurrentHP.text = m_Health.CurrentHealth.ToString();
        TotalHP.text = m_Health.MaximumHealth.ToString();
    }

    public void UpdateStamina()
    {
        Stamina.UpdateStaminaUI(m_Stamina.CurrentStamina, m_Stamina.MaxStamina);
    }

    public void OnEvent(LevelingEvents evetType)
    {
        switch (evetType.Type)
        {
            case LevelingEvents.EventType.LevelUp:
                LevelText.text = MatchManager.Instance.LevelSystem.Level.ToString();
                break;
            case LevelingEvents.EventType.Reset:
                LevelText.text = MatchManager.Instance.LevelSystem.Level.ToString();
                break;
        }
    }
}
