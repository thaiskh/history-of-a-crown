﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveJournalEntry : EventDialogue
{
    public JournalEntry Entry;
    
    public override void Trigger()
    {
        EventManager.TriggerEvent(new JournalEntryProgressEvent(Entry.Id));
        //DialogueManager.Instance.ExecuteNextNode(0);
        GUIManager.Instance.CloseDialogue();
    }

    public override void Skip()
    {
        EventManager.TriggerEvent(new JournalEntryProgressEvent(Entry.Id));
        GUIManager.Instance.CloseDialogue();
    }
}
