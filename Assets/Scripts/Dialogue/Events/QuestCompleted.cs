﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCompleted : DialogueCondition
{
    public override void Trigger()
    {
        NPCQuest npc = (graph as DialogueGraph).Trigger.GetComponent<NPCQuest>();
        List<Quest> quests = npc.Quests;
        foreach (var quest in quests)
        {
            if (quest != null)
            {
                if (QuestManager.Instance.QuestCompleted(quest))
                {
                    QuestManager.Instance.FinishQuest(quest);
                    Success = true;
                    break;
                }
                else
                {
                    Success = false;
                }
            }
        }

        base.Trigger();
    }
}
