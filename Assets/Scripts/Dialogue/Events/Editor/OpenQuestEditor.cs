﻿using UnityEditor;
using UnityEngine;
using XNodeEditor;

[CustomNodeEditor(typeof(OpenQuest))]
public class OpenQuestEditor : NodeEditor
{
    private SerializedProperty m_Quest;

    public override void OnBodyGUI()
    {
        serializedObject.Update();

        EventDialogue node = target as EventDialogue;

        NodeEditorGUILayout.PortField(target.GetInputPort("input"), UnityEngine.GUILayout.Width(100));

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 336;
    }
}
