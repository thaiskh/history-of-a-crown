﻿using System;

[Serializable]
public class DialogueSpeaker
{
   public enum DialogueSpeakerType {None, Player, ExternalAgent}

   public DialogueSpeakerType Speaker;
   public DialogueAgentData Data;

   public DialogueSpeaker(DialogueSpeakerType speaker, DialogueAgentData data)
   {
      Speaker = speaker;
      Data = data;
   }
}
