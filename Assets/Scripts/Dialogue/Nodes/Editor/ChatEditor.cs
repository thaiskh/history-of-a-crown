﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using XNode;
using XNodeEditor;

[CustomNodeEditor(typeof(Chat))]
public class ChatEditor : NodeEditor
{
    public override void OnBodyGUI()
    {
        serializedObject.Update();

        Chat node = target as Chat;
        
        if (node.Answers.Count == 0)
        {
            GUILayout.BeginHorizontal();
            NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"), GUILayout.MinWidth(0));
            NodeEditorGUILayout.PortField(GUIContent.none, target.GetOutputPort("output"), GUILayout.MinWidth(0));
            GUILayout.EndHorizontal();
        }
        else
        {
            NodeEditorGUILayout.PortField(GUIContent.none, target.GetInputPort("input"));
        }
        GUILayout.Space(-10);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("Text"), new GUIContent("Key"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("Speaker"), new GUIContent("Speaker"));

        EditorGUILayout.Space();
        NodeEditorGUILayout.InstancePortList("Answers", typeof(DialogueBaseNode), serializedObject, NodePort.IO.Output, Node.ConnectionType.Override);

        serializedObject.ApplyModifiedProperties();
    }

    public override int GetWidth()
    {
        return 300;
    }
}
