﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct AchievementProgressEvent
{
    public enum EventType
    {
        Talk, Stat, Kill, Die, Explore, Item, Interact
    }

    public EventType Type;
    public int ID;
    public int Amount;
    
    public AchievementProgressEvent(EventType type, int id, int amount = 0)
    {
        Type = type;
        ID = id;
        Amount = amount;
    }
}

public struct AchievementEvent
{
    public bool IsAchieved;
    public Achievement Achievement;

    public AchievementEvent(bool isAchieved, Achievement achievement)
    {
        IsAchieved = isAchieved;
        Achievement = achievement;
    }
}

public class AchievementManager : PersistentSingleton<AchievementManager>, EventListener<AchievementProgressEvent>
{
    public List<Achievement> AchievementsList;
    public List<AchievementStatus> Achievements { get; set; }

    protected override void Awake()
    {
        base.Awake();
        
        Achievements = new List<AchievementStatus>();
    }

    private void Start()
    {
        foreach (var achievement in AchievementsList)
        {
            AchievementStatus achievementStatus = Achievements.Find(x => achievement.Id == x.Achievement.Id);

            if (achievementStatus == null)
            {
                achievementStatus = new AchievementStatus(achievement);
                Achievements.Add(achievementStatus);
            }
        }
    }

    void OnEnable()
    {
        this.EventStartListening<AchievementProgressEvent>();
    }
    
    void OnDisable()
    {
        this.EventStopListening<AchievementProgressEvent>();
    }

    public AchievementStatus GetAchievement(int ID)
    {
        return Achievements.Find(x => x.Achievement.Id == ID);
    }

    public bool IsAchieved(Achievement achievement)
    {
        List<AchievementStatus> achieved = new AchievedFilter().Filter(Achievements);

        return achieved.Exists(x => x.Achievement.Id == achievement.Id);
    }
    
    private void KilledEnemy(int enemyID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateKillGoals(enemyID));
    }
    
    private void DeadByEnemy(int enemyID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateDieGoals(enemyID));
    }
    
    private void StatAmount(int statID, int amount)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateStatGoals(statID, amount));
    }
    
    private void ItemUsed(int itemID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateUseItemGoals(itemID));
    }

    private void ActivateNPC(int npcID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateNPCGoals(npcID));
    }
    
    private void ActivateZone(int zoneID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateZoneGoals(zoneID));
    }
    
    private void InteractTrigger(int triggerID)
    {
        List<AchievementStatus> accepted = new NonAchievedFilter().Filter(Achievements);
        accepted.ForEach(x => x.UpdateInteractGoals(triggerID));
    }

    public void OnEvent(AchievementProgressEvent eventType)
    {
        switch (eventType.Type)
        {
            case AchievementProgressEvent.EventType.Talk:
                ActivateNPC(eventType.ID);
                break;
            case AchievementProgressEvent.EventType.Explore:
                ActivateZone(eventType.ID);
                break;
            case AchievementProgressEvent.EventType.Stat:
                StatAmount(eventType.ID, eventType.Amount);
                break;
            case AchievementProgressEvent.EventType.Kill:
                KilledEnemy(eventType.ID);
                break;
            case AchievementProgressEvent.EventType.Die:
                DeadByEnemy(eventType.ID);
                break;
            case AchievementProgressEvent.EventType.Item:
                ItemUsed(eventType.ID);
                break;
            case AchievementProgressEvent.EventType.Interact:
                InteractTrigger(eventType.ID);
                break;
        }
    }
}

public interface AchievementFilter
{
    List<AchievementStatus> Filter(List<AchievementStatus> toFilter);
}

public class AchievedFilter : AchievementFilter
{
    public AchievedFilter()
    {

    }

    public List<AchievementStatus> Filter(List<AchievementStatus> toFilter)
    {
        return toFilter.Where(x => x.Progression.IsFinished).ToList();
    }
}

public class NonAchievedFilter : AchievementFilter
{
    public NonAchievedFilter()
    {

    }

    public List<AchievementStatus> Filter(List<AchievementStatus> toFilter)
    {
        return toFilter.Where(x => !x.Progression.IsFinished).ToList();
    }
}
